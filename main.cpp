#include <iostream>
#include <vector>
#include <NTL/ZZ.h>
#include <NTL/RR.h>
#include <fstream>
#include <string>

using namespace NTL;
using namespace std;

ZZ FkC1(long x, ZZ g , ZZ k0, ZZ k1){
    k1 = power(k1,x);
    k0=k0*k1;
    return power(g,to_long(k0));
}

void testFkC1(){
    ZZ k0=to_ZZ(2);
    ZZ k1=to_ZZ(1);
    long x = 3;
    ZZ g =to_ZZ(3) ;
    cout << "this is test"<< FkC1(x,g,k0,k1)<<endl;
}

ZZ tfunc( ZZ g0, ZZ g , ZZ a , ZZ c){
    return g0*power(g,to_long(a)*to_long(c));
}

typedef struct inputstr{
    ZZ x;
    vector<ZZ> c;
}inputstr;

inputstr readfile(){
    std::string STRING;
    ifstream infile;
    inputstr c;
    infile.open ("C:\\test\\Demo.txt");
    std::string labelx="[x]";
    while(std::getline(infile, STRING)) // To get you all the lines.
    {
        //cout<<"here";
        if (STRING.compare(labelx)==0)
        {
            std::getline(infile, STRING);
            const char *cstr = STRING.c_str();
            c.x=to_ZZ(cstr);
        }
        else{
            const char *cstr = STRING.c_str();
            ZZ coeff = to_ZZ(cstr);
            c.c.push_back(coeff);
        }

    }
    infile.close();
    return c;
}

int main()
{
    cout<<"----------------------Verifiable Computation Simulation-------------------------"<<endl;
    inputstr readstr = readfile();

    int m;
    long d = to_long(readstr.c.size()-1);

    ///////////////////////////////////////
    ZZ k0=to_ZZ(2);
    ZZ k1=to_ZZ(3);
    ZZ g=to_ZZ(2);
    ZZ a = to_ZZ(3);

    ZZ x=readstr.x;// This is input
    ////////////////////////////////////////
    //         set up vector c            //
    vector<ZZ> c;
    c=readstr.c;
    //         set up vector              //
    ////////////////////////////////////////

    ////////////////////////////////////////
    //         set up vector t            //
    vector<ZZ> t;
    long i =0;
    for(ZZ tmp:c){
        ZZ tmpt = tfunc(FkC1(i,g,k0,k1),g,a,tmp);
        t.push_back(tmpt);
        ++i;
    }
    //         set up vector              //
    ////////////////////////////////////////
    cout<<"----------------------This is keyGen stage-------------------------------"<<endl;
    cout<<"coeffienct c are <";
    for(ZZ tmp:c){
        cout<<tmp<<" ";
    }
    cout<<">"<<endl;

    cout<<"PK t are <";
    for(ZZ tmp:t){
        cout<<tmp<<" ";
    }
    cout<<">"<<endl;
    cout<<"----------------------This is Computing stage-------------------------------"<<endl;
    /*Simulate server part calculate y*/
    cout<<"calculate y"<<endl;
    vector<ZZ> dx;
    i =0;
    ZZ y = to_ZZ(0);
    for(ZZ tmp:c){
        ZZ tmpdx = power(x,i);
        dx.push_back(tmpdx);
        y = y+tmp*tmpdx;
        i++;
    }

    /*Compute t mutiplication*/
    ZZ T = to_ZZ(1);
    vector<ZZ>::iterator it1 = t.begin();
    vector<ZZ>::iterator it2 = dx.begin();
    for(;it1!=t.end();){
        T = T*power((*it1),to_long(*it2));
        ++it1;
        ++it2;
    }
    cout<<"Compute y is "
        <<y
        <<endl;
    cout<<"whether you want to cheat?"<<endl;
    cin>>y;
    cout<<"y is "<<y<<" now"<<endl;

    cout<<"----------------------Verify Stage-------------------------------"<<endl;
    /*Verify*/
    ZZ CFEval;
    CFEval = power(g ,to_long(k0*(1-power(k1*x,(d+1)))/(1-k1*x)));
    /*cout<<"CFEval is "
        <<CFEval
        <<endl;*/
    if(T == CFEval*power(g,to_long(y*a))){
        cout<<"valid solution"
            <<endl;
    }
    else{
        cout<<"fake solution"
            <<endl;
    }

    /*Verify*/

    cout<<"enter any number to exit"
        <<endl;
    cin >> m;
    return 0;
}
